// initialize express
const config = require('./config.json');

var data = require('./data.json');
var express = require('express');
var app = express();
var ExpressPeerServer = require('peer').ExpressPeerServer;
var options = {
    debug: true
};

var server = require('http').createServer(app);
app.use('/peerjs', ExpressPeerServer(server, options));
app.get('/config', function(req, res) {
    if(data[req.query.id]) {
        res.json(data[req.query.id]);
    } else {
        res.json({
            failed: true
        });
    }
});

server.listen(8878);
